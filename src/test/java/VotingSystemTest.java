import agents.DecisionAgent;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class VotingSystemTest {
	DecisionAgent agent = new DecisionAgent();


	@Test
	public void mseBasedVoting() {
		List<Pair<Double, Double>> predictions = new ArrayList<>();

		predictions.add(new ImmutablePair<>(-1D, 0.1));
		predictions.add(new ImmutablePair<>(1D, 0.5));
		predictions.add(new ImmutablePair<>(1D, 0.1));
		predictions.add(new ImmutablePair<>(0D, 0.2));
		predictions.add(new ImmutablePair<>(0D, 0.3));

		String finalResult = agent.getFinalDecision(predictions);

		Assertions.assertEquals("0.0", finalResult);
	}


	@Test
	public void majorityBaseVoting() {
		List<Pair<Double, Double>> predictions = new ArrayList<>();

		predictions.add(new ImmutablePair<>(1D, 0.1));
		predictions.add(new ImmutablePair<>(1D, 0.5));
		predictions.add(new ImmutablePair<>(1D, 0.1));
		predictions.add(new ImmutablePair<>(0D, 0.2));
		predictions.add(new ImmutablePair<>(0D, 0.3));

		String finalResult = agent.getFinalDecision(predictions);

		Assertions.assertEquals("1.0", finalResult);
	}


	@Test
	public void emptyVoting() {
		List<Pair<Double, Double>> predictions = new ArrayList<>();

		predictions.add(new ImmutablePair<>(-1D, 0.1));
		predictions.add(new ImmutablePair<>(-1D, 0.5));
		predictions.add(new ImmutablePair<>(-1D, 0.1));


		String finalResult = agent.getFinalDecision(predictions);

		Assertions.assertEquals("0.0", finalResult);
	}
}
