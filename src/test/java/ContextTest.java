import configuration.ConfigurationContext;
import configuration.Properties;
import configuration.impl.ConfigurationContextImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.WekaUtil;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ContextTest {
	private final ConfigurationContext ctx = ConfigurationContextImpl.loadContext();

	@Test
	public void assertMapIsLoadedProperly() throws NoSuchFieldException, IllegalAccessException {
		Assertions.assertEquals("src/main/resources/dataset/empty.txt", ctx.getProperty(Properties.PATH_TO_DATASET));
	}


	@Test
	public void wekatest() throws Exception {

		ConverterUtils.DataSource source = new ConverterUtils.DataSource("src/main/resources/dataset/audit.arff");
		Instances data = source.getDataSet();

		WekaUtil.reduceDatasetAttributesRandomly(data, 6);
		Collections.list(data.enumerateAttributes())
				.forEach(System.out::println);
	}


	@Test
	public void attributeRemover() throws Exception {
		ConverterUtils.DataSource source = new ConverterUtils.DataSource("src/main/resources/dataset/audit.arff");
		Instances data = source.getDataSet();

		Instance instance = data.get(0);
		int numOfAttribute = Collections.list(data.enumerateAttributes()).size();

		Instances single = new Instances(data, 1);
		single.add(0, instance);

		List<Integer> il1 = WekaUtil.pickNRandom(6, numOfAttribute, false);
		List<Integer> il2 = WekaUtil.pickNRandom(21, numOfAttribute, false);
		List<Integer> il3 = WekaUtil.pickNRandom(10, numOfAttribute, false);

		Instances i1 = WekaUtil.retainAttributes(data, il1);
		Instances i2 = WekaUtil.retainAttributes(data, il2);
		Instances i3 = WekaUtil.retainAttributes(data, il3);

		Assertions.assertEquals(6, Collections.list(i1.enumerateAttributes()).size());
		Assertions.assertEquals(21, Collections.list(i2.enumerateAttributes()).size());
		Assertions.assertEquals(10, Collections.list(i3.enumerateAttributes()).size());
		Assertions.assertEquals(1, single.size());
	}

	@Test
	public void checkAttributeMatch() throws Exception {
		List<String> bigList = Arrays.asList(
				"numbers", "RiSk_E", "Risk", "Money_Value", "Detection_Risk", "History", "Score_A", "Score_B", "Inherent_Risk", "Audit_Risk",
				"Risk_B", "Risk_C", "LOCATION_ID", "Risk_A", "Risk_F",
				"Sector_score", "PARA_B", "Risk_D", "PARA_A", "TOTAL", "District_Loss", "PROB", "Score", "CONTROL_RISK", "Score_MV"
		);
		List<String> smallList = Arrays.asList("PARA_B", "Risk_D", "PARA_A", "District_Loss", "RiSk_E"/*2*/, "Risk"/*3*/);
		List<Integer> match = WekaUtil.getMatchIndex(bigList, smallList);


	}

	@Test
	public void attributeRemove() throws Exception {
		ConverterUtils.DataSource source = new ConverterUtils.DataSource("src/main/resources/dataset/audit.arff");
		Instances data = source.getDataSet();
		Instance i = data.firstInstance();

		List<String> bigList = Arrays.asList(
				"numbers", "RiSk_E", "Risk", "Money_Value", "Detection_Risk", "History", "Score_A", "Score_B", "Inherent_Risk", "Audit_Risk",
				"Risk_B", "Risk_C", "LOCATION_ID", "Risk_A", "Risk_F",
				"Sector_score", "PARA_B", "Risk_D", "PARA_A", "TOTAL", "District_Loss", "PROB", "Score", "CONTROL_RISK", "Score_MV"
		);

		List<String> smallList = Arrays.asList("PARA_B"/*17*/, "Risk_D"/*18*/, "PARA_A"/*19*/, "District_Loss"/*21*/, "RiSk_E"/*2*/, "Risk"/*3*/);

		List<Integer> match = WekaUtil.getMatchIndex(bigList, smallList);

		Instances i1 = new Instances(data, 1);
		i1.add(0, i);

		i1 = WekaUtil.retainAttributes(i1, match);
		i = i1.firstInstance();


		ConverterUtils.DataSource source2 = new ConverterUtils.DataSource("src/main/resources/dataset/audit.arff");
		Instances data2 = source2.getDataSet();
		Instance iCopy = data2.firstInstance();

		double[] iArr = i.toDoubleArray();
		double[] iCopyArr = iCopy.toDoubleArray();

		Assertions.assertEquals(iArr[0], iCopyArr[1]);
		Assertions.assertEquals(iArr[1], iCopyArr[2]);
		Assertions.assertEquals(iArr[2], iCopyArr[16]);
		Assertions.assertEquals(iArr[3], iCopyArr[17]);
		Assertions.assertEquals(iArr[4], iCopyArr[18]);
		Assertions.assertEquals(iArr[5], iCopyArr[20]);
	}



}
