package agents;

import agents.behaviour.CyclicBehaviorWrapper;
import agents.behaviour.OneShotBehaviourWrapper;
import configuration.ConfigurationContext;
import configuration.Properties;
import configuration.impl.ConfigurationContextImpl;
import dto.JsonMessage;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Jackson;
import utils.MessageBuilder;
import utils.Registrar;
import utils.WekaUtil;
import weka.core.Instances;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.IntStream;

import static agents.Constants.*;


/**
 * Task:
 * 1. first load of configuration context [v]
 * 2. then load the arff file [v]
 * 3. split the the arr dataset [v]
 * 3.1 send it among agents
 */
public class MasterAgent extends Agent {
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	private ConfigurationContext ctx;
	private ContainerController controller;

	private int agentCount;
	private int trainingCounter;

	/**
	 * load first load of configuration context and register agent in DFservice
	 */
	protected void setup() {
		this.ctx = ConfigurationContextImpl.loadContext();
		this.controller = getContainerController();

		new Registrar<>(this).register(MASTER_AGENT);
		this.addBehaviour(new OneShotBehaviourWrapper(this, this::registerAgents));
		this.addBehaviour(new CyclicBehaviorWrapper(this, this::onCycle));
	}

	/**
	 * this is method will be wrapped in behavior and executed
	 *
	 * @param _ -> don't use it
	 * @return -> always return null !
	 */
	private Void registerAgents(Void _) {
		agentCount = Integer.parseInt(ctx.getProperty(Properties.AGENT_COUNT));
		trainingCounter = agentCount;
		List<Instances> data = WekaUtil.splitDataset(agentCount);


		IntStream.range(0, agentCount)
				// achtung maybe here we should send profile for Classifiers
				// eg.
				//	prod-> load existing models
				//	test-> train new models
				.mapToObj(index -> startAgents(CLASSIFIER_AGENT, CLASSIFIER_AGENT_PCK, data.get(index)))
				.forEach(ctx::registerAgentController);

		ctx.registerAgentController(startAgents(DECISION_AGENT, DECISION_AGENT_PCK, "empty"));
		logger.info("Agents are created, Agents are training");
		return null;
	}

	/**
	 * method which add agents into agent controller
	 *
	 * @param type   -> type of agent
	 * @param clazz  -> classpath to agent
	 * @param params -> params which should be passed to agent
	 * @return -> agent identifier and his controller
	 */
	private Pair<String, AgentController> startAgents(String type, String clazz, Object... params) {
		String id = type + "[" + UUID.randomUUID().toString() + "]";

		AgentController agentController = null;
		try {
			agentController = controller.createNewAgent(id, clazz, params);
			agentController.start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
			logger.error("Exception ");
		}
		return new ImmutablePair<>(id, agentController);
	}

	/**
	 * method which will listen for messages
	 */
	private Void onCycle(Void _) {

		Optional.ofNullable(receive()).ifPresent(msg -> {
			switch (msg.getPerformative()) {
				case ACLMessage.INFORM:
//					do we need to know when the training is done for agents?
					logger.debug("Received [Inform] Message::" + msg.toString());
					break;
				case ACLMessage.REQUEST:
					logger.info("Received [Request] Message -> Broadcasting it to agents");
					logger.debug("Received [Request] Message::" + msg.toString());
					onPredictionRequest(msg);
					break;
				default:
					logger.warn("[!] Unhandled message type " + msg.toString());
					break;
			}
		});
		return _;
	}

	/**
	 * when user agent need a prediction it will send message
	 * with Request type to master. Master will load Instance
	 * and it will send it to all classifier agents
	 * <p>
	 * expected message
	 * <p>
	 * {
	 * ..."metadata": {
	 * ......"predictionKey":"key"
	 * ....."prediction":0
	 * ...}
	 * ..."payload": [...]
	 * }
	 */
	private void onPredictionRequest(ACLMessage message) {
		JsonMessage dto = Jackson.deserialize(message.getContent(), JsonMessage.class);


		ACLMessage decisionAgentMessage = new MessageBuilder()
				.withMessageType(ACLMessage.SUBSCRIBE)
				.withReceiver(ctx.getFirstId(DECISION_AGENT))
				.withStringContent(dto.map.get("predictionKey"))
				.build();

		send(decisionAgentMessage);

		ctx.getAgentIds(CLASSIFIER_AGENT).forEach(agent -> {
			ACLMessage msg = new MessageBuilder()
					.withStringContent("Do predict")
					.withMessageType(ACLMessage.REQUEST)
					.withStringContent(message.getContent())
					.withReceiver(agent).build();

			send(msg);
		});


	}
}
