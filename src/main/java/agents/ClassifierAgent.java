package agents;

import agents.behaviour.CyclicBehaviorWrapper;
import agents.behaviour.OneShotBehaviourWrapper;
import configuration.ConfigurationContext;
import configuration.impl.ConfigurationContextImpl;
import dto.JsonMessage;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Jackson;
import utils.MessageBuilder;
import utils.Registrar;
import utils.WekaUtil;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static agents.Constants.*;

/**
 * Classifier Agents
 */

public class ClassifierAgent extends Agent {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    // this is synchronization flag
    // when training is done it will be set to true
    // to send a message to master agent that training is done
    private volatile boolean flag = false;
    // train instances for this agent
    private Instances data;
    //context where agent can access the properties of the system
    private ConfigurationContext ctx;
    // 6 attributes which different from agent to agent
    private List<String> attributes;
    //model
    private final J48 tree = new J48();
    // mse of current agent
    // it will be used in case we have ties
    private Double mseScore = null;

    @Override
    protected void setup() {
        // data processing
        this.data = (Instances) getArguments()[0];
        WekaUtil.reduceDatasetAttributesRandomly(this.data, 6);
        //set label for training
        this.data.setClassIndex(data.numAttributes() - 1);

        this.attributes = Collections.list(this.data.enumerateAttributes())
                .stream()
                .map(Attribute::toString)
                .collect(Collectors.toList());

        this.ctx = ConfigurationContextImpl.loadContext();


        //	JADE stuff
        new Registrar<>(this).register(CLASSIFIER_AGENT);
        this.addBehaviour(new OneShotBehaviourWrapper(this, this::startTraining));
        this.addBehaviour(new CyclicBehaviorWrapper(this, this::onCycle));

    }

    /**
     * this method should start training when its
     * it will save model and will set flag to true
     *
     * @param _ -> don't use it
     * @return -> always return null
     */
    private Void startTraining(Void _) {
        Pair<Instances, Instances> tmp = WekaUtil.toTrainAndValidation(data);
        Instances validation = tmp.getLeft();
        Instances train = tmp.getRight();
        train.setClassIndex(attributes.size());
        train.removeIf(Instance::classIsMissing);
        try {
            tree.buildClassifier(train);
            evaluateModel(train, validation, tree);
        } catch (Exception e) {
            e.printStackTrace();
        }

        flag = true;
        return null;
    }

	private void evaluateModel(Instances train, Instances validationSet, Classifier classifier) throws Exception {
		// evaluate classifier and print some statistics
		Evaluation eval = new Evaluation(train);
		eval.evaluateModel(classifier, validationSet);
		this.mseScore = eval.rootMeanSquaredError();

		logger.debug(eval.toSummaryString("\nResults\n======\n", false));
	}


    public Void onCycle(Void _) {
        if (flag) {
            onTrainingDone();
            //set it back to false
            //to not enter again this if again
            flag = false;
        }

        Optional.ofNullable(receive()).ifPresent(msg -> {
            switch (msg.getPerformative()) {
                // we get
                case ACLMessage.REQUEST:
                    //logger.info("Received [Request] from [" + msg.getSender().getName() + "]");
                    logger.debug("Received [Request] Message::" + msg.toString());
                    classify(msg);
                    break;
                default:
                    logger.warn("[!] Unhandled message type " + msg.toString());
                    break;
            }
        });
        return _;
    }

    /**
     * notify master that training is done
     * NOTICE: not sure if we need if
     */
    private void onTrainingDone() {
        logger.trace("Training is done");
        ACLMessage msg = new MessageBuilder()
                .withStringContent("Training Done")
                .withMessageType(ACLMessage.INFORM)
                .withReceiver(MASTER_AGENT).build();
        send(msg);
    }

    /**
     * load Instance(which we have to classify) from message
     * check if incoming 20 attributes of instance contains
     * 6 attributes of training set. if yes it does classification
     * if not it sends -1
     *
     * @param message -> message from master with payload
     */
    private void classify(ACLMessage message) {
        JsonMessage dto = Jackson.deserialize(message.getContent(), JsonMessage.class);
        Instance toPredict = new DenseInstance(dto.payload.weight, dto.payload.values.stream().mapToDouble(x -> x).toArray());

        if (dto.payload.attributes.containsAll(attributes)) {

            //we need this for building instances entity
            List<Attribute> atr = dto.payload.attributes.stream().map(Attribute::new).collect(Collectors.toList());
            //this is list of attributes(their indexes in data) to be retained
            List<Integer> attributesIndex = WekaUtil.getMatchIndex(dto.payload.attributes, attributes);

            //build a temp Instances because we can remove attributes
            //all at once only using Instances
            Instances tempInstances = new Instances("name", new ArrayList<>(atr), 1);
            tempInstances.add(0, toPredict);

            try {
                tempInstances = WekaUtil.retainAttributes(tempInstances, attributesIndex);
                toPredict.setDataset(data);
                sendPrediction(dto.map.get("predictionKey"), tree.classifyInstance(toPredict));
            } catch (Exception e) {
                e.printStackTrace();
            }

		} else {
//			logger.info("Unable to do prediction for [" + dto.map.get("predictionKey") + "] attributes doesn't match ");
            sendPrediction(dto.map.get("predictionKey"), -1D);
        }


    }

    /**
     * should send prediction to decision agent
     *
     * @param predictionKey -> unique identifier of prediction received from MASTER agent
     * @param prediction    -> prediction value
     */
    private void sendPrediction(String predictionKey, Double prediction) {
        logger.debug("Prediction[" + predictionKey + "] -> [" + prediction + "]");
        JsonMessage json = new JsonMessage();
        json.withAttributes(
                new ImmutablePair<>("predictionKey", predictionKey),
                new ImmutablePair<>("prediction", prediction.toString()),
                new ImmutablePair<>("mse", this.mseScore.toString())
        );
        ACLMessage msg = new MessageBuilder()
                .withJsonContent(json)
                .withReceiver(ctx.getFirstId(DECISION_AGENT))
                .withMessageType(ACLMessage.INFORM)
                .build();

        send(msg);

    }


}

