package agents;

import agents.behaviour.CyclicBehaviorWrapper;
import agents.behaviour.TickerBehaviourWrapper;
import configuration.ConfigurationContext;
import configuration.Properties;
import configuration.impl.ConfigurationContextImpl;
import dto.InstanceWrapper;
import dto.JsonMessage;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Jackson;
import utils.MessageBuilder;
import utils.Registrar;
import utils.WekaUtil;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static agents.Constants.MASTER_AGENT;
import static agents.Constants.USER_AGENT;

/**
 * 1. request prediction []
 * 2. print prediction []
 */
public class UserAgent extends Agent {
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	private int PREDICTION_REQUEST;
	private int counter ;
	private  ConfigurationContext ctx;
	private final List<Integer> actualLabels = new ArrayList<>(PREDICTION_REQUEST);
	private final List<Integer> predictedLabels = new ArrayList<>(PREDICTION_REQUEST);


	@Override
	protected void setup() {
		this.ctx = ConfigurationContextImpl.loadContext();
		this.PREDICTION_REQUEST = Integer.parseInt(ctx.getProperty(Properties.PREDICTIONS_REQUESTS));
		this.counter = PREDICTION_REQUEST;
		new Registrar<>(this).register(USER_AGENT);
		this.addBehaviour(new CyclicBehaviorWrapper(this, this::onCycle));
		this.addBehaviour(new TickerBehaviourWrapper(this, this::requestPrediction, 10000));

	}

	private Void onCycle(Void _) {
		Optional.ofNullable(receive())
				.ifPresent(msg -> {
					switch (msg.getPerformative()) {
						case ACLMessage.INFORM:
							//results of prediction
							this.onPredictionReceived(msg);
							break;
						default:
							logger.warn("[!] Unhandled message type " + msg.toString());
							break;
					}
				});
		return _;
	}


	private void onPredictionReceived(ACLMessage message) {
		JsonMessage dto = Jackson.deserialize(message.getContent(), JsonMessage.class);
		String key = dto.map.get("predictionKey");
		String prediction = dto.map.get("prediction");
		logger.info("Received prediction for key [{}] with value [{}]",key,prediction);
		predictedLabels.add(Math.round(Float.parseFloat(prediction)));
	}

	/**
	 * 1. Randomly pick one entity from test
	 * 2. delete label
	 * 3. reduce attribute number to 20
	 * 4. send it to master and require classification
	 **/
	private Void requestPrediction(Void _) {
		if (counter != 0) {
			String predictionKey = UUID.randomUUID().toString();

			Instances tmp = WekaUtil.getRandomTestInstance();
			Instance a = tmp.get(0);
			Double w = a.weight();
			Integer label = (int) a.toDoubleArray()[24];
			logger.info("[" + counter + "][" + predictionKey + "] -> [" + label + "]");
			this.actualLabels.add(label);
			tmp.deleteAttributeAt(24);
			WekaUtil.reduceDatasetAttributesRandomly(tmp, 20);

			//convert weka instance to double array because it be sent to
			//classification agents
			List<Double> x = DoubleStream.of(a.toDoubleArray()).boxed().collect(Collectors.toList());


			// list of attributes is save and sent along with above double list
			//to classification agent because agents have to match attributes
			List<String> attribute = Collections.list(a.enumerateAttributes())
					.stream()
					.map(Attribute::toString)
					.collect(Collectors.toList());

			InstanceWrapper instance = new InstanceWrapper(w, x, attribute);
			JsonMessage json = new JsonMessage(instance);

			json.withAttributes(
					new ImmutablePair<String, String>("predictionKey", predictionKey)
			);

			ACLMessage msg = new MessageBuilder()
					.withJsonContent(json)
					.withReceiver(MASTER_AGENT)
					.withMessageType(ACLMessage.REQUEST)
					.build();
			send(msg);

			counter--;
		}  else {
			int correctPredicted = IntStream.range(0, PREDICTION_REQUEST)
					.map(index -> actualLabels.get(index).equals(predictedLabels.get(index)) ? 1 : 0)
					.boxed().mapToInt(Integer::intValue).sum();

			float score = (((float) correctPredicted)/((float) PREDICTION_REQUEST))*100;

			logger.info("Accuracy score of the MAS is [{}%]",score);
		}
		return _;
	}

}
