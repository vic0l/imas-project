package agents.behaviour;

import jade.core.Agent;

import jade.core.behaviours.TickerBehaviour;

import java.util.function.Function;

/**
 * Wrapper for avoiding boilerplate code in Agents
 */
public class CyclicBehaviorWrapper extends TickerBehaviour {
	private final Function<Void, Void> callback;

	public CyclicBehaviorWrapper(Agent agent, Function<Void, Void> callback) {
		super(agent,500);
		this.callback = callback;
	}

	@Override
	protected void onTick() {
		this.callback.apply(null);
	}

}
