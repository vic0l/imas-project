package agents.behaviour;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

import java.util.function.Function;

/**
 * Wrapper for avoiding boilerplate code in Agents
 */
public class OneShotBehaviourWrapper  extends OneShotBehaviour {
	private final Function<Void, Void> callback;

	public OneShotBehaviourWrapper(Agent agent, Function<Void, Void> callback) {
		super(agent);
		this.callback = callback;
	}
	@Override
	public void action() {
		this.callback.apply(null);
	}
}
