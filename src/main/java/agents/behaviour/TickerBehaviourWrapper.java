package agents.behaviour;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

import java.util.function.Function;

/**
 * Wrapper for avoiding boilerplate code in Agents
 */
public class TickerBehaviourWrapper extends TickerBehaviour {
	private final Function<Void, Void> callback;

	public TickerBehaviourWrapper(Agent agent, Function<Void, Void> callback, long period) {
		super(agent, period);
		this.callback = callback;
	}

	@Override
	protected void onTick() {
		this.callback.apply(null);
	}

}
