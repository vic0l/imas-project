package agents;

import agents.behaviour.CyclicBehaviorWrapper;
import configuration.Properties;
import configuration.impl.ConfigurationContextImpl;
import dto.JsonMessage;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Jackson;
import utils.MessageBuilder;
import utils.Registrar;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static agents.Constants.DECISION_AGENT;
import static agents.Constants.USER_AGENT;

/**
 * 1. aggregate results from Classifiers []
 * 2. send final decision to UserAgent []
 */
public class DecisionAgent extends Agent {

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	private int numberOfClassifiers;
	//
	//	keep the id of the request and results from classifiers
	//	so on each iteration it will check if any id have values.size == numberOfClassifiers
	//	Pair<Double, Double> -> left is the prediction right is the mse score
	//
	private ConcurrentHashMap<String, List<Pair<Double, Double>>> auditMap;

	@Override

	protected void setup() {
		this.auditMap = new ConcurrentHashMap<>();
		this.numberOfClassifiers = Integer.parseInt(
				ConfigurationContextImpl.loadContext().getProperty(Properties.AGENT_COUNT)
		);

		new Registrar<>(this).register(DECISION_AGENT);
		this.addBehaviour(new CyclicBehaviorWrapper(this, this::onCycle));
	}

	private Void onCycle(Void _) {

		Optional.ofNullable(receive())
				.ifPresent(msg -> {
					switch (msg.getPerformative()) {
						case ACLMessage.INFORM:
							//message from classifiers
							//with prediction results
							logger.debug("Received [Prediction]" + msg.toString());
							onNewInform(msg);
							break;
						case ACLMessage.SUBSCRIBE:
							//message from master
							//which will subscribe DecisionAgent
							//to a prediction request
							onNewPredictSubscribe(msg);
							break;
						default:
							logger.warn("[!] Unhandled message type " + msg.toString());
							break;
					}

				});


		return _;
	}


	/**
	 * when user agent will require a new prediction
	 * we will receive id of that prediction because
	 * we need to know to how aggregate different predictions
	 */
	private void onNewPredictSubscribe(ACLMessage message) {
		String subscriptionId = message.getContent();
		logger.info("Registered new prediction [" + subscriptionId + "]");
		this.auditMap.put(subscriptionId, new ArrayList<>());
	}

	/**
	 * when a classifier finish a prediction it send
	 * a message with prediction and prediction id
	 */
	private void onNewInform(ACLMessage message) {

		JsonMessage dto = Jackson.deserialize(message.getContent(), JsonMessage.class);

		String key = dto.map.get("predictionKey");
		Double prediction = Double.valueOf(dto.map.get("prediction"));
		Double mseOfAgent = Double.valueOf(dto.map.get("mse"));

		auditMap.get(key).add(new ImmutablePair<>(prediction, mseOfAgent));
		logger.info("Received [" + key + "] with value [" + dto.map.get("prediction") + "]");


		if (auditMap.get(key).size() == this.numberOfClassifiers) {
			onClassifiersCompletePrediction(key);
		}
	}

	/**
	 * when we have results from all classifiers
	 * we aggregate them and send to user agent
	 */
	private void onClassifiersCompletePrediction(String predictionKey) {

		String finalDecision = this.getFinalDecision(this.auditMap.get(predictionKey));

		JsonMessage json = new JsonMessage();
		json.withAttributes(
				new ImmutablePair<>("prediction", finalDecision),
				new ImmutablePair<>("predictionKey", predictionKey)
		);


		ACLMessage msg = new MessageBuilder()
				.withReceiver(USER_AGENT)
				.withMessageType(ACLMessage.INFORM)
				.withJsonContent(json)
				.build();

		send(msg);
		auditMap.remove(predictionKey);

	}

	/**
	 * 	this is voting protocol
	 *	each agent is sending his mse score, we use this score in case we have ties
	 *	so if we have next cases:
	 *
	 *  [1,0] -> here we have tie so in this case we will pick vote of the agent which
	 *  have lowest mse, because that agent has higher precision
	 *
	 * @param predictions  list  with prediction where left-> is prediction, right -> is the mse score
	 * @return final decision of voting protocol
	 */
	public String getFinalDecision(List<Pair<Double, Double>> predictions) {
		// 1. clean
		// if Attributes doesn't match for Classification agent it sends  -1
		// so we have remove these values and keep only the values which matched
		List<Pair<Double, Double>> cleanPredictions = predictions
				.stream()
				.filter(val -> val.getLeft() != -1D)
				.collect(Collectors.toList());

		if (cleanPredictions.isEmpty()) {
			// no attribute matches
			return "-1";
		}

		Comparator<Map.Entry<String, List<Double>>> comparator = (e1, e2) -> {
			return Integer.compare(e1.getValue().size(), e2.getValue().size());
		};

		// try majority voting
		List<Map.Entry<String, List<Double>>> counting = cleanPredictions
				.stream()
				.map(Pair::getLeft)
				.collect(Collectors.groupingBy(String::valueOf))
				.entrySet()
				.stream()
				//sorting is done asc
				.sorted(comparator)
				.collect(Collectors.toList());


		//case where all classifiers voted in same way
		if (counting.size() == 1) {
			return counting.get(0).getKey();
		}

		// compare size of votes between 0 & 1 with are equal majority voting is impossible
		// use mse voting
		if (counting.get(0).getValue().size() == counting.get(1).getValue().size()) {
			//return with lowest mse
			List<Map.Entry<String, Double>> mseList = cleanPredictions.stream()
					.collect(
							Collectors.groupingBy(
									pair -> pair.getLeft().toString(), Collectors.summingDouble(Pair::getRight)
							)
					).entrySet()
					.stream()
					.sorted((e1, e2) -> Double.compare(e1.getValue(), e2.getValue()))
					.collect(Collectors.toList());


			logger.info("Voting based on MSE [" +
					mseList.get(0).getKey() + "(" + mseList.get(0).getValue() + ")]  and " +
					mseList.get(1).getKey() + "(" + mseList.get(1).getValue() + ")]");

			return mseList.get(0).getKey();

		}
		//because sorting is done asc we reverse the list
		//to get the highest
		Collections.reverse(counting);
		return counting.get(0).getKey();
	}

}
