package agents;


interface Constants {
	String MASTER_AGENT = "master";
	String DECISION_AGENT = "DecisionAgent";
	String USER_AGENT = "UserAgent";
	String CLASSIFIER_AGENT = "ClassifierAgent";

	String MASTER_AGENT_PCK = "agents.MasterAgent";
	String CLASSIFIER_AGENT_PCK = "agents.ClassifierAgent";
	String DECISION_AGENT_PCK = "agents.DecisionAgent";
	String USER_AGENT_PCK = "agents.UserAgent";
}
