package configuration.impl;

import configuration.ConfigurationContext;
import configuration.Properties;
import jade.wrapper.AgentController;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class ConfigurationContextImpl implements ConfigurationContext {
	private final Map<Properties, String> map;
	private final Map<String, AgentController> controllerMap;
	private static ConfigurationContext instance;

	private ConfigurationContextImpl() {
		this.controllerMap = new HashMap<>();
		this.map = new ConfigurationLoaderImpl()
				.getConfiguration();
	}

	@Override
	public String getProperty(Properties key) {
		return this.map.get(key);
	}

	@Override
	public void registerAgentController(Pair<String, AgentController> pair) {
		this.controllerMap.put(pair.getLeft(), pair.getRight());
	}

	@Override
	public List<String> getAgentIds(String agentType) {
		return controllerMap.keySet()
				.stream().filter(str -> str.contains(agentType))
				.collect(Collectors.toList());
	}

	@Override
	public String getFirstId(String agentType) {
		return this.getAgentIds(agentType).stream().findFirst().get();
	}

	@Override
	public List<AgentController> getControllers(){
		return this.controllerMap
				.values().stream()
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	public static synchronized ConfigurationContext loadContext() {
		if (instance == null) {
			instance = new ConfigurationContextImpl();
		}
		return instance;
	}
}
