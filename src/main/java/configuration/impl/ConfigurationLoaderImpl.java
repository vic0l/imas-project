package configuration.impl;

import configuration.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConfigurationLoaderImpl {
	private final static String PATH_TO_CONFIG = "src/main/resources/properties.xml";
	private final static String ROOT_XML_NODE = "properties";
	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());


	/**
	 * load the configuration file from converts the properties nodes
	 * into a HashMap and keep them in memory
	 *
	 * @return Hashmap with properties
	 */
	Map<Properties, String> getConfiguration() {
		logger.info("Loading configuration file");
		Map<Properties, String> map = new HashMap<Properties, String>();
		try {
			File configurationFile = new File(PATH_TO_CONFIG);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(configurationFile);
			doc.getDocumentElement().normalize();
			NodeList propertiesList = doc.getElementsByTagName(ROOT_XML_NODE).item(0).getChildNodes();
			map = toMap(propertiesList);
		} catch (Exception ex) {
			logger.error("Exception occurred during the loading of configuration", ex);
			System.exit(2);
		}
		return map;

	}

	/**
	 * this function should map xml file to Hash map and return it
	 *
	 * @param xmlNodes node <properties>
	 * @return Hashmap with properties
	 */
	private Map<Properties, String> toMap(NodeList xmlNodes) {
		return IntStream.range(0, xmlNodes.getLength())
				.mapToObj(xmlNodes::item)
				.filter(Node::hasChildNodes)
				.map(this::toMapEntry)
				.collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
	}


	/**
	 * parse this type of nodes and return as a MapEntry
	 * <property>
	 * <name>some_important_property</name>
	 * <value>src/main/resources/files/empty.txt</value>
	 * </property>
	 *
	 * @param xmlElement is property node
	 * @return Map entry
	 */
	private AbstractMap.SimpleEntry<Properties, String> toMapEntry(Node xmlElement) {
		List<Node> elements = IntStream.range(0, xmlElement.getChildNodes().getLength())
				.mapToObj(index -> xmlElement.getChildNodes().item(index))
				.filter(node -> node.getNodeType() == Node.ELEMENT_NODE)
				.collect(Collectors.toList());

		return new AbstractMap.SimpleEntry<>(
				Properties.PATH_TO_DATASET.fromValue(elements.get(0).getTextContent()), elements.get(1).getTextContent()
		);


	}
}
