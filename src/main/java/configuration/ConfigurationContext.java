package configuration;


import jade.wrapper.AgentController;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * ConfigurationContext interface which will keep properties
 *
 * This class has to purposes:
 * 	1. 	it keeps the property of project in context and keep
 * 		them accessible for each object/agent
 *
 *  2.  because in our architecture each agent has an unique
 *  	identifier (uuid) we need somewhere to save the
 *  	in case we want to sent the message to one certain agent
 *
 */
public interface ConfigurationContext {

	/**
	 * should return value of property by name where:
	 *
	 * @param key -> is the key of property
	 * @return -> is the  value of property
	 */

	String getProperty(Properties key);

	/**
	 * should register agent controller and keep their
	 *
	 * @param pair -> identifier and instance
	 */
	void registerAgentController(Pair<String, AgentController> pair);

	/**
	 * this method return ids of agents in jade platform (when you need to send message)
	 * for ex you need to send message to classifier agents
	 */
	List<String> getAgentIds(String agentType);


	/**
	 *	return agent identifier (they might more then one in the list) by the type of the agent
	 *  if we need decision agent we just pass {DecisionAgent} and we an identifier of an decision agent
	 *  and we will get his identifier for jade
	 */
	String getFirstId(String agentType);

	/**
	 * returns all controllers of agents
	 * used when the system is finished the job
	 * and all agents have to closed
	 */
	List<AgentController> getControllers();

}
