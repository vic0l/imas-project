package configuration;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * represent required properties for our
 * MAS/Jade project
 */
public enum Properties {

	PATH_TO_DATASET("path_to_dataset"),
	AGENT_COUNT("number_of_classifiers_agents"),
	PREDICTIONS_REQUESTS("prediction_request");

	private final String xmlValue;

	Properties(String xmlValue) {
		this.xmlValue = xmlValue;
	}

	public String getXmlValue() {
		return this.xmlValue;
	}


	/**
	 * return enum from .xml value
	 */
	public Properties fromValue(String value) {
		return Arrays.stream(Properties.values())
				.collect(Collectors.toMap(
						Properties::getXmlValue,
						prop -> prop
				)).get(value);
	}


}
