package dto;

import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;

/**
 *  representation of json message which is sent between agents
 */
public class JsonMessage implements Serializable {

	public InstanceWrapper payload;
	public HashMap<String, String> map;


	public JsonMessage() {
		this.map = new HashMap<>();
	}

	/**
	 * overloaded constructor with param
	 * @param payload wrapper for weka instance when send it for classification
	 */
	public JsonMessage(InstanceWrapper payload) {
		this.payload = payload;
		this.map = new HashMap<>();

	}


	/**
	 * this function adds properties to the json
	 * we can add them as many as we want they are dynamic
	 * so each agent should which params he is able to extract from the map
	 *
	 * @param props arguments which are passed to agents
	 */
	public void withAttributes(Pair<String, String>... props) {
		Arrays.stream(props).forEach(x ->
				this.map.put(x.getLeft(), x.getRight())
		);
	}

}
