package dto;

import java.util.List;


/**
 * this class is wrapper for weka instances
 * we are sending them between agents in json format using this wrapper
 *
 */
public class InstanceWrapper {
	public Double weight;
	public List<Double> values;
	public List<String> attributes;

	public InstanceWrapper(Double weight, List<Double> values, List<String> attributes) {
		this.weight = weight;
		this.values = values;
		this.attributes = attributes;
	}

	//only for jackson usage
	public InstanceWrapper() {

	}
}
