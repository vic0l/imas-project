package utils;


import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * class which register agents in JADE platform
 * @param <T> instance of agent
 */
public class Registrar<T extends Agent> {
	private final DFAgentDescription agentDescription;
	private final ServiceDescription serviceDescription;
	private final Agent agent;

	private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	public Registrar(T agent) {
		this.agentDescription = new DFAgentDescription();
		this.serviceDescription = new ServiceDescription();
		this.agent = agent;
	}

	private DFAgentDescription buildDescription(String type) {
		//Classifier agents must register to the Directory Facilitator providing the information
		//about the services they can provide.
		serviceDescription.setType(type);
		serviceDescription.setName(agent.getName());
		serviceDescription.setOwnership("MAS");
		agentDescription.setName(agent.getAID());
		agentDescription.addServices(serviceDescription);
		return agentDescription;
	}

	public void register(String type) {
		try {
			DFService.register(agent, buildDescription(type));
		} catch (FIPAException e) {
			logger.info(jade.util.Logger.SEVERE + " Cannot register with DF", e);
			agent.doDelete();
		}
		logger.info("Agent with type [" + type + "] registered ");
	}

}

