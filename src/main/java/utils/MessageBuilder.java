package utils;

import dto.JsonMessage;
import jade.core.AID;
import jade.lang.acl.ACLMessage;


import java.io.IOException;
import java.util.Optional;


public class MessageBuilder {
	private int messageType;
	private String receiver;
	private String content;
	private JsonMessage request;

	public MessageBuilder() {

	}

	public MessageBuilder withMessageType(int messageType) {
		this.messageType = messageType;
		return this;
	}

	public MessageBuilder withReceiver(String receiver) {
		this.receiver = receiver;
		return this;
	}

	public MessageBuilder withStringContent(String content) {
		this.content = content;
		return this;
	}

	public MessageBuilder withJsonContent(JsonMessage request) {
		this.request = request;
		return this;
	}

	public ACLMessage build() {
		ACLMessage msg = new ACLMessage(this.messageType);
		msg.addReceiver(new AID(this.receiver, AID.ISLOCALNAME));
		Optional.ofNullable(this.content).ifPresent(msg::setContent);

		if (request != null) {
			try {
				msg.setContent(Jackson.instance.writeValueAsString(request));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return msg;
	}
}
