package utils;

import configuration.Properties;
import configuration.impl.ConfigurationContextImpl;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemovePercentage;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * class which keeps all weka related code
 *
 */
public class WekaUtil {

	public static Instances testInstances;

	private static Instances loadDataset() {
		ConverterUtils.DataSource source = null;
		Instances data = null;
		try {
			source = new ConverterUtils.DataSource(
					ConfigurationContextImpl.loadContext().getProperty(Properties.PATH_TO_DATASET)
			);
			data = source.getDataSet();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}


	/**
	 * will get from data set {@param chunks} of instances
	 * with 300 samples each
	 */
	public static List<Instances> splitDataset(int chunks) {
		Instances allData = loadDataset();
		testInstances = new Instances(allData);
		Collections.shuffle(allData);
		RemovePercentage percentage = new RemovePercentage();

		testInstances.addAll(allData.subList(0, 50));

		List<Instance> trainingData = allData.subList(50, allData.size());


		List<Instances> instancesChunks = IntStream.range(0, chunks)
				.mapToObj(x -> new Instances(allData, 300))
				.collect(Collectors.toList());

		instancesChunks.parallelStream().forEach(instance -> {
			pickNRandom(300, trainingData.size(), true)
					.forEach(index -> {
						instance.add(trainingData.get(index));
					});
		});

		return instancesChunks;
	}

	/**
	 * it split the {@param data} into 2 instance on is for training
	 * and second is for validation
	 *
	 * left -> is validation set
	 * right -> is train set;
	 */
	public static Pair<Instances, Instances> toTrainAndValidation(Instances data) {
		int validationIndex = Double.valueOf(data.size() * 0.25).intValue();
		Instances validation = new Instances(data, validationIndex);
		Instances train = new Instances(data, (data.size() - validationIndex));
		IntStream.range(0, validationIndex).forEach(index -> validation.add(data.get(index)));
		IntStream.range(validationIndex, data.size()).forEach(index -> train.add(data.get(index)));
		return new ImmutablePair<>(validation, train);
	}


	/**
	 * self explanatory
	 * @param data -> actual data on which we want to reduce attributes
	 * @param desiredNumOfAttributes -> number of attributes to retain
	 */
	public static void reduceDatasetAttributesRandomly(Instances data, Integer desiredNumOfAttributes) {
		int temp = ((data.numAttributes() - desiredNumOfAttributes) - 1);
		IntStream.range(0, temp)
				.forEach(x -> {
					int random = new Random().nextInt(data.numAttributes() - 1);
					data.deleteAttributeAt(random);
				});

	}

	/**
	 * util for getting random {@param nDesired } from a range which
	 * to {@param n}
	 * @param startZero in case we want to start range from 1
	 * @return retained range
	 */
	public static List<Integer> pickNRandom(int nDesired, int n, boolean startZero) {
		//attribute remover throw error on 0 index (retain function)
		int start = startZero ? 0 : 1;

		List<Integer> v = IntStream.range(start, n).boxed().collect(Collectors.toList());
		Collections.shuffle(v);
		return v.subList(0, nDesired);
	}

	/**
	 * @param instances  instances on which we want to perform attribute retaintion
	 * @param retainList list with integers indexes of attributes
	 */
	public static Instances retainAttributes(Instances instances, List<Integer> retainList) throws Exception {
		String retain = null;

		//
		//	attributes are not indexed from 0
		//	thats why we have this helper if which increase all elements with 1
		//
		if (retainList.stream().findFirst().get() == 0) {
			retain = retainList.stream()
					.mapToInt(value -> value + 1)
					.boxed()
					.map(String::valueOf)
					.collect(Collectors.joining(","));
		} else {

			retain = retainList.stream()
					.map(String::valueOf)
					.collect(Collectors.joining(","));
		}

		Remove remove = new Remove();
		remove.setAttributeIndices(retain);
		remove.setInvertSelection(true);
		remove.setInputFormat(instances);
		return Filter.useFilter(instances, remove);
	}

	/**
	 * this function will return list with index from list
	 * for example we have list as big list {a,b,c,d}
	 * and as short list we put short list {b,c} then the result will be {1,2}
	 */
	public static List<Integer> getMatchIndex(List<String> bigList, List<String> shortList) {
		return IntStream.range(0, bigList.size())
				.filter(index -> shortList.contains(bigList.get(index)))
				.map(it -> it += 1)
				.boxed()
				.collect(Collectors.toList());
	}


	public static Instances getRandomTestInstance() {
		ArrayList<Attribute> attributes = Collections.list(testInstances.enumerateAttributes());
		Instances tmp = new Instances("randomInstances", attributes, 1);
		int randomIndex = new Random().nextInt(testInstances.size() - 1);
		Instance i = testInstances.get(randomIndex);
		tmp.add(0, i);
		return tmp;
	}
}
