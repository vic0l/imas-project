package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * wrapper class for Jackson library for
 * avoiding boilerplate code across agents
 */
public class Jackson {
	public static final ObjectMapper instance = new ObjectMapper();

	/**
	 * function which reads the json and convert it into object
	 *
	 * @param json string value which represent object in json format
	 * @param clazz type of class in which jackson will convert the json
	 * @param <T> return type
	 *
	 * @return object representation
	 */
	public static <T> T deserialize(String json, Class<T> clazz) {
		T obj = null;
		try {
			obj = instance.readValue(json, clazz);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return obj;
	}
}
