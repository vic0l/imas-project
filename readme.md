# IMAS Project

## Introduction

Our implementation of MAS system, which consists of 4 agents:
- **User Agent**: acts as consumer of our multi agent system it
  sends the request to the system analyze and collect them and
  when no more request are left to send it will print the results
  of system
- **Master Agent**: base agent of system it acts as mediator between
  user agent and rest of agents also his second responsibility is
  to start a certain amount of classifier agents, passed as a property
  `properties.xml` file.
- **Classifier Agent** : is the agent which is performing the classification
  at start-up each agent has 300 instances on which it learns and then
  when it receives an instance to predict it performs the prediction based
  on received data.
- **Decision Agent** : this agent aggregate predictions of each classifier
  agent and get a final result from it.




## Cooperation mechanism
![cooperation mechanism](./imgs/img.png)


## Run the project
In order to run the project just run the next command:

```shell
sh ./run.sh
```
it contains all maven-tasks required for building the project
and running it. Use this command when you run the project for the
first time or for  development purpose otherwise use next command:
```shell
mvn -Prun-mas exec:java
```
using this command the project won't be cleaned and rebuild again
it will just run previously build jar.


### Properties
in order to set a different path to data set or change the amount of
classifier agents change update the properties file located in `src/resources/properties.xml`



